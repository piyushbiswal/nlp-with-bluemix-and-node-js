'use strict';
const http = require('http'),
    express = require('express'),
    app = express(),
    translator = require('./utils/translator.js');


let PORT = process.env.PORT || 9000
// converting english word to spanish 

app.get('/translate/:sourcelang/:targetlang/:sentence', function (req, res, next) {
    let sentence = req.params.sentence,
        targetlang = req.params.targetlang.toLowerCase(), // target language 
        sourcelang = req.params.sourcelang.toLowerCase(); // source language
    translator.Translate(sourcelang, targetlang, sentence)
        .then((result) => {
            let responseData = {
                success: true,
                payload: {
                    result: result,
                    from: sourcelang,
                    to: targetlang
                }
            }

            res.status(200).send(responseData);
        }).
        catch((err) => {
            next(err);
        })
})

app.use((err, res, req) => {
    if (err) {
        res.status(404).json({ Error: err });
    }
})

app.listen(PORT, () => {
    console.log('server started at port: ', PORT);
});

