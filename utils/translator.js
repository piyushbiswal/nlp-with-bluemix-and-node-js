'use strict';

let languageTranslator = require('watson-developer-cloud/language-translator/v2');
// waston NLP configurations      

let config = {
    "url": "https://gateway.watsonplatform.net/language-translator/api",
    "username": "8e4a8dbf-d577-4c7c-95af-8ca27cf30c01",
    "password": "Wga0dnOkJjo"
}


let langRef = {
    'english': 'en',
    'spanish': 'es'
}

let translate = new languageTranslator({
    username: process.env.username || config.username,
    password: process.env.password || config.password,
    url: process.env.url || config.url
});


let Translate = (sourcelang, targetlang, sentence) => {
    // translating functionality
    return new Promise((resolve, reject) => {
        translate.translate({
            text: sentence,
            source: langRef[sourcelang],
            target: langRef[targetlang]
        }, (err, translation) => {
            if (err) {
                return reject(err);
            }
            else {
                return resolve(translation['translations'][0].translation)
            }
        });
    })

}

module.exports = {
    Translate
}      